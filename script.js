window.addEventListener('scroll', function() {
    var navbar = document.getElementById('fh5co-header');
    console.log('Script is working!');

    if (window.scrollY > 50) {
      navbar.classList.add('scrolled');
    } else {
      navbar.classList.remove('scrolled');
    }
  });
//   function toggleMenu() {
//     var navLinks = document.getElementById("navLinks");
//     var burger = document.querySelector(".burger");
//     burger.classList.toggle("change");
//     if (navLinks.style.display === "block") {
//       navLinks.style.display = "none";
//     } else {
//       navLinks.style.display = "block";
//     }
//   }
  
//   document.getElementById('navbar-toggler').addEventListener('click', function() {
//     this.classList.toggle('active');
//     console.log('Script is working here!');
//     document.getElementById('nav').classList.toggle('active');
//   });

  const form = document.getElementById("contact-form");

  form.addEventListener("submit", (e) => {
    e.preventDefault();
  
    const formData = new FormData(form);
    const name = formData.get("name");
    const email = formData.get("email");
    const subject = formData.get("subject");
    const message = formData.get("message");
    const phone = formData.get("phone");
  
    const to = "namrataruchandani21@gmail.com";
    const headers = new Headers();
    headers.append("Content-Type", "text/plain; charset=UTF-8");
    headers.append("From", email);
    // headers.append("Access-Control-Allow-Origin", "*");
    const messageText = `Name: ${name}\nEmail: ${email}\nSubject: ${subject}\nPhone: ${phone}\n\n${message}`;
  
    fetch("mailto:" + to, {
      method: "POST",
      headers: headers,
      body: messageText,
    })
      .then((response) => {
        if (response.ok) {
          form.reset();
          alert("Message sent successfully!");
        } else {
          throw new Error("Error sending message.");
        }
      })
      .catch((error) => {
        console.error(error);
        alert("Error sending message.");
      });
  });
  